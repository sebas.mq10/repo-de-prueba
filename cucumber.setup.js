const { Module } = require('module');
const css = require('css');
const fs = require('fs');
const { JSDOM } = require('jsdom');

//TODO: refactorizar y poner explicacion

const DEFAULT_HTML = '<html><body><div id="container"></div></body></html>';

global.window = new JSDOM(DEFAULT_HTML, {/*JSDOM Setup*/}).window;
global.document = window.document;
global.navigator = {
	userAgent: 'node.js'
};
const {require: oldRequire} = Module.prototype;
const handler = {
	get: function(target,prop,receiver){
		return 'fake';
	}
};

Module.prototype.require = function hijacked(file) {
	if(file.endsWith('.css')){
		const f = fs.readFileSync('src/'+file.substring(1),'utf-8');
		const ast = css.parse(f);
		if(ast.stylesheet.parsingErrors.length > 0)
			throw new Error(file + ': no es CSS valido');
		return new Proxy(ast,handler);
	}
	return oldRequire.apply(this, arguments);
};
