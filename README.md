Este es un template de [NextJS](https://nextjs.org/) (framework de [React](https://reactjs.org/)) y [TypeScript](https://www.typescriptlang.org/) pre-configurado con tests BDD con [cucumber-js](https://cucumber.io/docs/installation/javascript/), unit tests con [vitest](https://vitest.dev/). Reemplazar esta descripcion por la descripcion de tu aplicacion, reemplazar 'mi-proyecto' y 'mi-grupo' en el README

Este repositorio incluye las dependencias:
- [Chai](https://www.chaijs.com/)
    - libreria de aserciones, para comparar valores en el contexto de tests
- [Substitute](https://www.npmjs.com/package/@fluffy-spoon/substitute)
    - libreria para mocks stubs y fakes ([ejemplos](https://www.tabnine.com/code/javascript/functions/%40fluffy-spoon%2Fsubstitute/any))
- [testing-library/react](https://testing-library.com/docs/react-testing-library/intro/)
- [testing-library/jest-dom](https://testing-library.com/docs/ecosystem-jest-dom/)
    - herramientas adicionales para testear componentes React y elementos del DOM
- [Cucumber](https://cucumber.io/docs/installation/javascript/)
    - libreria de testing BDD. ([guias](https://cucumber.io/docs/guides/))
- [vitest](https://vitest.dev/)
    - libreria de testing
- [eslint](https://eslint.org/)
    - linter
- [ts-node](https://www.npmjs.com/package/ts-node)
    - ejecutor de typescript

---

## Primeros pasos

- crear un repositorio vacio en Gitlab ()
    TODO

- clonar el repositorio, a una carpeta con el nombre de la aplicacion
    ```bash
    git clone git@gitlab.com:ms-templates/temporal mi-proyecto
    ```

- Apuntar el repositorio clonado a nuestro repositorio git
    ```bash
    # pararse en la carpeta de proyecto
    cd mi-proyecto
    # cambiar la ruta del "origen"
    git remote set-url origin git@gitlab.com:mi-grupo/mi-proyecto
    ```

- cambiar la propiedad `"name"` del archivo `package.json` por el nombre de tu aplicacion
    ```json
    // ...mi-proyecto/package.json
    {
        "name": "mi-proyecto",
        ...
    }
    ```

- correr el comando `yarn` para instalar todas las dependencias
    ```bash
    # para habilitar el uso de yarn (si yarn ya esta habilitado en el sistema, este paso se puede saltear)
    corepack enable
    # es equivalente a 'yarn install'
    yarn
    ```
- Pushear a nuestro repo
    ```bash
    # la opcion '-u' es igual a '--set-upstream'
    git push -u origin main
    ```

- En Gitlab, marcar la rama 'main' como protegida

    TODO

- En Gitlab, habilitar los "Merge Requests"

    TODO

---

## Correr la aplicacion

```bash
# para ejecutar en modo de desarrollo, la app en ejecucion se actualiza solo con cambios a los archivos
yarn dev
```

```bash
# para compilar la aplicacion y startearla en modo de produccion
yarn build
# seguido de...
yarn start
```

---

## Desarrollo de la aplicacion

1. Crear un branch nuevo del repositorio, con el nombre de la feature
    ```bash
    # checkeamos que estemos parados en la branch 'main' y que no haya cambios sin commitear
    git status
    # en caso de no estar en la rama main, vamos a ella
    git checkout main
    # recibimos los ultimos cambios del servidor
    git pull
    # finalmente creamos el branch 
    git checkout -b una-feature-de-ejemplo
    ```
2. [Descubrimiento](TODO):  Comenzar con la descripcion de un feature en lenguaje [Gherkin](TODO) en la carpeta `...mi-proyecto/features/`. Commitear un mensaje que comienze con "nueva feature:". (Luego de subir la rama al servidor, [abrir el MR en Gitlab](TODO))
    ```gherkin
    # features/nuevaFeature.feature
    Feature: nueva feature
        una feature nueva que hace cosas
            
    ```

    ```bash
    # agregar los cambios a 'staging'
    git add .
    # commitear 
    git commit -m "nuevo feature: ejemplo de feature"
    # como la rama no esta creada en el servidor, la tenemos que pushear con la opcion --set-upstream
    git push -u origin una-feature-de-ejemplo
    # una vez pusheado podemos ir a abrir el MR
    ```
3. [RED - BDD](TODO): 
    - Agregarle a la feature creada en el paso anterior, un solo escenario que se deba cumplir de la feature, marcar los step definitions de dicho scenario tirando un error descriptivo en ellos. 
    ```gherkin
    # features/nuevaFeature.feature
    Feature: nueva feature
        una feature nueva que hace cosas

        Scenario: hace una cosa
            Given una cosa
            When pasa algo
            Then la cosa se hace
    ```
    ```ts
    // steps/nuevaFeature.step.ts
    import {Given,When,Then} from "@cucumber/cucumber"
    Given('una cosa',function(){
        throw new Error('no hay tal cosa');
    })
    When('pasa algo',function(){
        throw new Error('no pasa nada');
    })
    Then('la cosa se hace',function(){
        throw new Error('no se hace nada');
    })
    ```
    

TODO

## Explicacion de los archivos del template
    TODO