import {Given,When,Then} from '@cucumber/cucumber';
import assert from 'node:assert';
import { expect } from 'vitest';

Given('a script to run acceptances tests', function () {
	this.miMundo = 'es redondo';
});


When('the developer runs the script', function () {
	this.miMundo += ' y grande';
});


Then('the acceptance test output is shown', function () {
	assert.equal(this.miMundo,'es redondo y grande');
});
