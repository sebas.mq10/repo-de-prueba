import { Given,When,Then, AfterAll } from '@cucumber/cucumber';
import {render,screen,cleanup} from '@testing-library/react';
import { expect } from 'chai';
import Home from '@/pages/index';

AfterAll(function (){
	cleanup();
});

Given('A React component', function () {
	render(Home());
});


Then('it should display the word {string}', async function (string) {
	const h1 = await screen.getByText('hola',{exact:false});
	expect(h1.innerHTML).to.include(string);
});


Given('A React component that i can click', function () {
	this.componente = 'sarasa';
});


When('I click it', function () {
	this.componente += ' las cosas';
});


Then('I can check the component was clicked', function () {
	expect(this.componente).to.be.equal('sarasa las cosas');
});
