let common = [
	'--publish-quiet',
	'features/**/*.feature', // Specify our feature files
	'--require cucumber.setup.js', // Load cucumber setup file
	'--require-module ts-node/register', // Load TypeScript module
	'--require steps/**/*.ts', // Load step definitions
	'--format progress-bar', // Load custom formatter
].join(' ');
  
module.exports = {
	default: common
};