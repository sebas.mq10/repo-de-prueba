Feature: Test UI components with react

    This repository should be able to run tests against React components

    Scenario: I can detect text displayed in components
        Given A React component
        Then it should display the word "Hola"

    Scenario: I can trigger a button press
        Given A React component that i can click
        When I click it
        Then I can check the component was clicked