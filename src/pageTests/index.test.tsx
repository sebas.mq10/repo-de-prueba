import {expect,test} from 'vitest';
import {render,screen} from '@testing-library/react';

const X:React.FC = () => <div>hola chau</div>;


test('that i can test a react component',async ()=>{
	render(<X/>);
	const div = await screen.getByText('hola chau');
	expect(div.innerHTML).toContain('chau');
});